[![Build status](https://git.depad.fr/depad/d3/badges/master/build.svg)](https://git.depad.fr/depad/d3/pipelines)


# docker-compose.yml sample with Traefik
```yml
version: "3"

services:

  mysql:
    env_file: .env-mysql
    container_name: mysql
    image: mariadb:latest
    restart: always
    volumes:
      - /opt/docker/d3/mysql:/var/lib/mysql
    networks:
      - back 

  d3:
    env_file: .env-d3
    container_name: d3
    image: registry.depad.fr/depad/d3
    restart: always
    depends_on:
      - mysql
    networks:
      - back 
      - traefik_gateway
    labels:
      - "traefik.enable=true"
      - "traefik.docker.network=traefik_gateway"
      - "traefik.protocol=http"
      - "traefik.port=80"
      - "traefik.backend=d3"
      - "traefik.frontend.rule=Host:d3.domain.tld"

networks:
  traefik_gateway:
    external: true
  back:
    driver: bridge

```
# vars

set vars into `.env` from `.env-test`
