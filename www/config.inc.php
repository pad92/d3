<?php
date_default_timezone_set('Europe/Paris');
setlocale(LC_ALL, array('fr_FR.UTF-8','fr_FR@euro','fr_FR','french'));
$html_ttl = gmdate('D, d M Y H:i:s \G\M\T', time() + 60);
$date_start_s1  = '2014-08-29 21:00:00';
$date_end_s1    = '2015-02-01 16:00:00';
$date_start_s2  = '2015-02-13 18:00:00';
$date_end_s2    = '2015-04-05 21:00:00';
$date_start_s3  = '2015-04-10 18:00:00';
$date_end_s3    = '2015-08-23 17:00:00';
$date_start_s4  = '2015-08-28 17:00:00';
$date_end_s4    = '2015-12-30 17:00:00';
$date_start_s5  = '2016-01-15 17:00:00';
$date_start_s12 = '2017-11-12 17:00:00';

$date_start_cur = $date_start_s12;


$btags_fun = ['ATA#2828','BeN#2887','Oxilium#2548','baldur#2717', 'lolo#2956', 'Deker#2311', 'Tiamat#2232', 'wam#2632', 'Daii#2242', 'Guillou#2817', 'Ichigo95#2126', 'Pllaag#2423', 'titoof57#2762', 'ZacksFairy#2896', 'OKi#2225', 'Lilith#2317', 'Mudak#2504','ImPad#2692','Zwentibold#2984','oli33140#2291','Evilior#2388'];
$btags_friends = ['ImPad#2692' , 'NunuxTux#2234', 'Kastu#1652','Kaliya#2105','mrsx15#2232','Nunu#2309','Pandatomik#1588','Sogox#2929','aryuthere#2811','Hiubiah#2201','Yarchou#2917','Cous#1746','hecian#2310','Bu1ent#2317','zen6#2305','Azufell#2198','Aeb#2682','ore#2158'];

$youtube_username = '';
$twitch_url = 'https://api.twitch.tv/kraken/streams?channel=pad92';

$gitlab_rss       = 'https://git.depad.fr/depad/d3/commits/master.atom';

?>
