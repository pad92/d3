<?php
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && header('HTTP/1.0 403 Forbidden') &&  exit;

header("Content-Type: text/plain");
#error_reporting(E_ALL);
#ini_set('display_errors', true);
$_SERVER['DOCUMENT_ROOT'] = dirname(__FILE__);
require_once ('config.inc.php');
require_once ('config.db.inc.php');
require_once ('libs/diablo3.api.class.php');

$mysqli = new mysqli($mysql_host, $mysql_user, $mysql_pass, $mysql_bdd);
/* Vérification de la connexion */
if (mysqli_connect_errno()) {
    printf("Échec de la connexion : %s\n", mysqli_connect_error());
    exit();
}

$btags     = array_unique(array_merge($btags_fun, $btags_friends));
$career_datas     = array();

foreach ($btags as $btag) {
    $d3      = new Diablo3($btag, 'eu', 'fr_FR');
    $CAREER_DATA  = $d3->getCareer();
    if(is_array($CAREER_DATA)) {
        foreach($CAREER_DATA['heroes'] as $heroe) {
            // creation des heros manquant
            if ($mysqli_result = $mysqli->query('SELECT `id` FROM `hero_data` WHERE `id` = '.$heroe['id'])) {
                if ($mysqli_result->num_rows == 0) {
                    $mysqli_result->close();
                    $query = 'INSERT INTO `hero_data` (battleTag, id, seasonal, seasonCreated ,name, level, hardcore, gender, dead, class)
                        VALUES ( "'.$CAREER_DATA['battleTag'].'" , '.$heroe['id'].', '.$heroe['paragonLevel'].', '.((bool)$heroe['seasonal'] === TRUE ? 0 : 1 ).', "'.$heroe['name'].'", '.$heroe['level'].', '.((bool)$heroe['hardcore'] === TRUE ? 0 : 1 ).', "'.((bool)$heroe['gender'] === TRUE ? 'female' : 'male' ).'", '.((bool)$heroe['dead'] === TRUE ? 0 : 1 ).', "'.$heroe['class'].'" );';
                    $mysqli->query($query);
                }
            }
        }
        // fetch profile datas
        $career_datas[] = array (
            'battleTag'                     => $CAREER_DATA['battleTag'],
            'lastUpdated'                   => $CAREER_DATA['lastUpdated'],
            'paragonLevel'                  => $CAREER_DATA['paragonLevel'],
            'paragonLevelHardcore'          => $CAREER_DATA['paragonLevelHardcore'],
            'paragonLevelSeason'            => $CAREER_DATA['paragonLevelSeason'],
            'paragonLevelSeasonHardcore'    => $CAREER_DATA['paragonLevelSeasonHardcore'],
            'killsMonsters'                 => $CAREER_DATA['kills']['monsters'],
            'killsElites'                   => $CAREER_DATA['kills']['elites'],
            'killsHardcoreMonsters'         => $CAREER_DATA['kills']['hardcoreMonsters']
        );
    };
};

// update profiles
foreach( $career_datas as $row ) {
    $career_insert = 'INSERT INTO `career_data` (battleTag, lastUpdated, paragonLevel, paragonLevelHardcore, paragonLevelSeason, paragonLevelSeasonHardcore, killsMonsters, killsElites, killsHardcoreMonsters)
                VALUES ("'.$row['battleTag'].'", FROM_UNIXTIME('.$row['lastUpdated'].'), '.$row['paragonLevel'].', '.$row['paragonLevelHardcore'].', '.$row['paragonLevelSeason'].', '.$row['paragonLevelSeasonHardcore'].', '.$row['killsMonsters'].', '.$row['killsElites'].', '.$row['killsHardcoreMonsters'].');';
    $mysqli->query($career_insert);
    unset($career_insert);
}

// update heroes

$mysqli_herosid = $mysqli->query('SELECT `id`, `battleTag`, `last-updated` FROM `hero_data` WHERE `dead` <> 0');
while ($heroid = $mysqli_herosid->fetch_assoc()) {
    $d3 = new Diablo3($heroid['battleTag'], 'eu', 'fr_FR');
    $heros_datas=$d3->getHero($heroid['id']);
    $level                      =@$heros_datas['level'];
    $hardcore                   =@$heros_datas['hardcore'];
    $seasonal                   =@$heros_datas['seasonal'];
    $seasonCreated              =@$heros_datas['seasonCreated'];
    $skills_active_0_skill_icon =@$heros_datas['skills']['active']['0']['skill']['icon'];
    $skills_active_1_skill_icon =@$heros_datas['skills']['active']['1']['skill']['icon'];
    $skills_active_2_skill_icon =@$heros_datas['skills']['active']['2']['skill']['icon'];
    $skills_active_3_skill_icon =@$heros_datas['skills']['active']['3']['skill']['icon'];
    $skills_active_4_skill_icon =@$heros_datas['skills']['active']['4']['skill']['icon'];
    $skills_active_5_skill_icon =@$heros_datas['skills']['active']['5']['skill']['icon'];
    $skills_passive_0_skill_icon=@$heros_datas['skills']['passive']['0']['skill']['icon'];
    $skills_passive_1_skill_icon=@$heros_datas['skills']['passive']['1']['skill']['icon'];
    $skills_passive_2_skill_icon=@$heros_datas['skills']['passive']['2']['skill']['icon'];
    $skills_passive_3_skill_icon=@$heros_datas['skills']['passive']['3']['skill']['icon'];
    $skills_active_0_skill_slug =@$heros_datas['skills']['active']['0']['skill']['slug'];
    $skills_active_1_skill_slug =@$heros_datas['skills']['active']['1']['skill']['slug'];
    $skills_active_2_skill_slug =@$heros_datas['skills']['active']['2']['skill']['slug'];
    $skills_active_3_skill_slug =@$heros_datas['skills']['active']['3']['skill']['slug'];
    $skills_active_4_skill_slug =@$heros_datas['skills']['active']['4']['skill']['slug'];
    $skills_active_5_skill_slug =@$heros_datas['skills']['active']['5']['skill']['slug'];
    $skills_passive_0_skill_slug=@$heros_datas['skills']['passive']['0']['skill']['slug'];
    $skills_passive_1_skill_slug=@$heros_datas['skills']['passive']['1']['skill']['slug'];
    $skills_passive_2_skill_slug=@$heros_datas['skills']['passive']['2']['skill']['slug'];
    $skills_passive_3_skill_slug=@$heros_datas['skills']['passive']['3']['skill']['slug'];
    $dead                       =@$heros_datas['dead'];
    $lastUpdated                =@$heros_datas['last-updated'];
    if ($lastUpdated != $heroid['last-updated']) {
        $mysqli_update="UPDATE `depad_d3`.`hero_data` SET
                    `level` = '$level',
                    `hardcore` = '$hardcore',
                    `seasonal` = '$seasonal',
                    `seasonCreated` = '$seasonCreated',
                    `skills_active_0_skill_icon` = '$skills_active_0_skill_icon',
                    `skills_active_1_skill_icon` = '$skills_active_1_skill_icon',
                    `skills_active_2_skill_icon` = '$skills_active_2_skill_icon',
                    `skills_active_3_skill_icon` = '$skills_active_3_skill_icon',
                    `skills_active_4_skill_icon` = '$skills_active_4_skill_icon',
                    `skills_active_5_skill_icon` = '$skills_active_5_skill_icon',
                    `skills_passive_0_skill_icon` = '$skills_passive_0_skill_icon',
                    `skills_passive_1_skill_icon` = '$skills_passive_1_skill_icon',
                    `skills_passive_2_skill_icon` = '$skills_passive_2_skill_icon',
                    `skills_passive_3_skill_icon` = '$skills_passive_3_skill_icon',
                    `skills_active_0_skill_slug` = '$skills_active_0_skill_slug',
                    `skills_active_1_skill_slug` = '$skills_active_1_skill_slug',
                    `skills_active_2_skill_slug` = '$skills_active_2_skill_slug',
                    `skills_active_3_skill_slug` = '$skills_active_3_skill_slug',
                    `skills_active_4_skill_slug` = '$skills_active_4_skill_slug',
                    `skills_active_5_skill_slug` = '$skills_active_5_skill_slug',
                    `skills_passive_0_skill_slug` = '$skills_passive_0_skill_slug',
                    `skills_passive_1_skill_slug` = '$skills_passive_1_skill_slug',
                    `skills_passive_2_skill_slug` = '$skills_passive_2_skill_slug',
                    `skills_passive_3_skill_slug` = '$skills_passive_3_skill_slug',
                    `dead` = '$dead',
                    `last-updated` = FROM_UNIXTIME($lastUpdated) WHERE `hero_data`.`id` = ".$heroid['id'];
        if ($mysqli->query($mysqli_update) === FALSE) {
        	$mysqli_update="UPDATE `depad_d3`.`hero_data`
		    SET `dead` = 1,
                    WHERE `hero_data`.`id` = ".$heroid['id'];
		$mysqli->query($mysqli_update);
        }
        unset($mysqli_update);
    }
    unset($heros_datas);
}
$mysqli_herosid->free();
$mysqli->close();

?>
