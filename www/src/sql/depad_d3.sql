-- --------------------------------------------------------

--
-- Table structure for table `career_data`
--

DROP TABLE IF EXISTS `career_data`;
CREATE TABLE IF NOT EXISTS `career_data` (
  `battleTag` varchar(20) COLLATE utf8_bin NOT NULL,
  `lastUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `paragonLevel` smallint(6) DEFAULT NULL,
  `paragonLevelHardcore` smallint(6) DEFAULT NULL,
  `paragonLevelSeason` smallint(6) DEFAULT NULL,
  `paragonLevelSeasonHardcore` smallint(6) NOT NULL,
  `killsMonsters` int(11) DEFAULT NULL,
  `killsElites` int(11) DEFAULT NULL,
  `killsHardcoreMonsters` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `clan`
--

DROP TABLE IF EXISTS `clan`;
CREATE TABLE IF NOT EXISTS `clan` (
  `clan_id` int(5) NOT NULL,
  `clan_tag` varchar(10) COLLATE utf8_bin NOT NULL,
  `clan_name` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `hero_data`
--

DROP TABLE IF EXISTS `hero_data`;
CREATE TABLE IF NOT EXISTS `hero_data` (
  `battleTag` varchar(20) COLLATE utf8_bin NOT NULL,
  `id` int(11) NOT NULL,
  `name` varchar(20) COLLATE utf8_bin NOT NULL,
  `class` enum('barbarian','crusader','witch-doctor','demon-hunter','monk','wizard') COLLATE utf8_bin DEFAULT NULL,
  `gender` enum('male','female') COLLATE utf8_bin NOT NULL,
  `level` smallint(6) NOT NULL,
  `hardcore` tinyint(1) DEFAULT NULL,
  `seasonal` tinyint(1) DEFAULT NULL,
  `seasonCreated` tinyint(3) DEFAULT NULL,
  `skills_active_0_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_1_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_2_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_3_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_4_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_5_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_0_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_1_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_2_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_3_skill_icon` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_0_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_1_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_2_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_3_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_4_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_active_5_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_0_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_1_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_2_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `skills_passive_3_skill_slug` varchar(35) COLLATE utf8_bin NOT NULL,
  `dead` tinyint(1) DEFAULT NULL,
  `last-updated` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(5) NOT NULL,
  `clan_id` int(5) DEFAULT NULL,
  `google_id` decimal(21,0) NOT NULL,
  `google_name` varchar(60) COLLATE utf8_bin NOT NULL,
  `google_email` varchar(60) COLLATE utf8_bin NOT NULL,
  `google_link` varchar(60) COLLATE utf8_bin NOT NULL,
  `google_picture_link` varchar(60) COLLATE utf8_bin NOT NULL,
  `battleTag` varchar(20) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `career_data`
--
ALTER TABLE `career_data`
  ADD PRIMARY KEY (`lastUpdated`,`battleTag`);

--
-- Indexes for table `clan`
--
ALTER TABLE `clan`
  ADD PRIMARY KEY (`clan_id`);

--
-- Indexes for table `hero_data`
--
ALTER TABLE `hero_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `class` (`class`,`gender`,`level`,`hardcore`,`seasonal`,`seasonCreated`,`dead`,`last-updated`),
  ADD FULLTEXT KEY `battleTag` (`battleTag`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `google_id` (`google_id`),
  ADD KEY `clan_id` (`clan_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clan`
--
ALTER TABLE `clan`
  MODIFY `clan_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `clan_fk` FOREIGN KEY (`clan_id`) REFERENCES `clan` (`clan_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
