<?php
error_reporting(E_ALL);
ini_set('display_errors', true);
require_once ('config.inc.php');
require_once ('config.db.inc.php');
require_once ('libs/diablo3.api.class.php');
require_once ('vendor/autoload.php');
use Ghunti\HighchartsPHP\Highchart;
use Ghunti\HighchartsPHP\HighchartJsExpr;
header('Expires: '.$html_ttl);

$twitch = json_decode(@file_get_contents($twitch_url), true);
$twitch_online = FALSE;
if(!empty($twitch)){
    foreach($twitch['streams'] as $mydata){
        if($mydata['_id'] != null){
            $title  = 'Twitch - '.$twitch_game;
            $twitch_name = $mydata['channel']['display_name'];
            $twitch_game = $mydata['channel']['game'];
            $twitch_url  = $mydata['channel']['url'];
            $twitch_online = TRUE;
        }
    }
}

$q = explode('?', str_replace('/', '_', ltrim($_SERVER['REQUEST_URI'], '/')));
if (!empty($_GET['time'])) {
    switch($_GET['time']) {
    case 'day':     $delay = $_GET['time'];     break;
    case 'week':    $delay = $_GET['time'];     break;
    case 'month':   $delay = $_GET['time'];     break;
    default:        $delay = 'month';   break;
    }
} else { $delay = 'month'; }
$page_type= FALSE;
switch ($q[0]) {
case (preg_match("/^clan-fun(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = '&#60;FUN&#62; Maraudeurs';
    $btags = $btags_fun;
    natcasesort($btags);
    break;
case (preg_match("/^friends(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Friends';
    $btags = $btags_friends;
    natcasesort($btags);
    break;
case (preg_match("/^youtube(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Youtube';
    $xml = simplexml_load_file(sprintf('http://gdata.youtube.com/feeds/base/users/%s/uploads?alt=rss&v=2&orderby=published', $youtube_username));
    if ( ! empty($xml->channel->item[0]->link) ) {
        parse_str(parse_url($xml->channel->item[0]->link, PHP_URL_QUERY), $url_query);
        if ( ! empty($url_query['v']) )
            $youtube_lastvid = $url_query['v'];
    }
    break;
case (preg_match("/^twitch(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    if($twitch_online == TRUE){
        $title  = 'Twitch - Offline';
    } else {
        $twitch_online = FALSE;
    }
    break;
case (preg_match("/^stats_level_soft(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Niveaux : Softcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, paragonLevel
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
case (preg_match("/^stats_level_soft-s(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Niveaux : Saison Softcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, paragonLevelSeason
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
          AND `lastUpdated` > '$date_start_cur'
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
case (preg_match("/^stats_level_hard(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Niveaux : Hardcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, paragonLevelHardcore
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
case (preg_match("/^stats_level_hard-s(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Niveaux : Saison Hardcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, paragonLevelSeasonHardcore
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
          AND `lastUpdated` > '$date_start_cur'
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
case (preg_match("/^stats_kills_soft(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Kills Softcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, killsMonsters
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
case (preg_match("/^stats_kills_hard(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
    $title = 'Kills Hardcore';
    $sql_query = "SELECT DISTINCT YEAR(`lastUpdated`) AS lastUpdated_year, MONTH(`lastUpdated`) AS lastUpdated_month, DAY(`lastUpdated`) AS lastUpdated_day, HOUR(`lastUpdated`) AS lastUpdated_hour, battleTag, MINUTE(`lastUpdated`) AS lastUpdated_minute, battleTag, killsHardcoreMonsters
        FROM `career_data`
        WHERE `lastUpdated` > timestampadd($delay, -2, now())
        ORDER BY `lastUpdated` ASC";
    $page_type = 'stats';
    break;
default:
    $default = TRUE;
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Diablo III<?php if (isset($title)) { echo ' - '.$title;} ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Diablo III stat">
        <meta name="author" content="Pascal A.">
        <meta name="generator" content="vim">
        <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/darkly/bootstrap.min.css" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Inconsolata' rel='stylesheet' type='text/css'>
        <link href="/static/style.css" rel="stylesheet">
        <meta name="theme-color" content="#375a7f">
    </head>
    <body style data-twttr-rendered="true">
        <div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                        <a href="/" class="navbar-brand">Diablo III</a>
                    </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li><a href="/clan-fun.html"><i class="fa fa-users"></i> &#60;FUN&#62; Maraudeurs</a></li>
                            <li><a href="/friends.html"><i class="fa fa-users"></i> Friends</a></li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-line-chart"></i> Profiles<span class="caret"></span></a>
                              <ul class="dropdown-menu" role="menu">
                                <li class="dropdown-header">Niveaux</li>
                                <li><a href="/stats/level/soft.html">Paramon Softcore</a></li>
                                <li><a href="/stats/level/soft-s.html">Paramon Softcore Saison</a></li>
                                <li><a href="/stats/level/hard.html">Paramon Hardcore</a></li>
                                <li><a href="/stats/level/hard-s.html">Paramon Hardcore Saison</a></li>
                                <li class="divider"></li>
                                <li class="dropdown-header">Kills</li>
                                <li><a href="/stats/kills/soft.html">Softcore</a></li>
                                <li><a href="/stats/kills/hard.html">Hardcore</a></li>
                              </ul>
                            </li>
<!--
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-line-chart"></i> Persos<span class="caret"></span></a>
                              <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Softcore</a></li>
                                <li><a href="#">Softcore Saison</a></li>
                                <li><a href="#">HardCore</a></li>
                                <li><a href="#">HardCore Saison</a></li>
                              </ul>
                            </li>
-->
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if ($twitch_online == TRUE) {?><li><a href="/twitch"><i class="fa fa-twitch"></i></a></li><?php } ?>
                            <li><a href="/youtube.html"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
<?php
if (isset($title)) {
    echo '<h1>'.$title.'</h1>';
}
if ($page_type == 'stats') {

    try {
        $pdo = new PDO ("mysql:host=$mysql_host;dbname=$mysql_bdd","$mysql_user","$mysql_pass");
    } catch (PDOException $e) {
        echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        exit;
    }

    $query = $pdo->prepare($sql_query);
    $query->execute();
    switch ($q[0]) {
case (preg_match("/^stats_level_soft(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Niveaux';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['paragonLevel'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevel']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevel']));
            }
        }
        break;
case (preg_match("/^stats_level_soft-s(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Niveaux';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['paragonLevelSeason'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelSeason']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelSeason']));
            }
        }
        break;
case (preg_match("/^stats_level_hard(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Niveaux';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['paragonLevelHardcore'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelHardcore']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelHardcore']));
            }
        }
        break;
case (preg_match("/^stats_level_hard-s(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Niveaux';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['paragonLevelSeasonHardcore'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelSeasonHardcore']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['paragonLevelSeasonHardcore']));
            }
        }
        break;
case (preg_match("/^stats_kills_soft(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Kills';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['killsMonsters'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['killsMonsters']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['killsMonsters']));
            }
        }
        break;
case (preg_match("/^stats_kills_hard(|\.html)$/i", $q[0]) ? $q[0] : !$q[0]):
        $chart_y_title = 'Kills';
        foreach  ($query as $row) {
            $battleTags[]       = $row['battleTag'];
            $battleTags         = array_unique($battleTags);
            $lastUpdated_date   = (int)$row['lastUpdated_year'].', '.(int)($row['lastUpdated_month']-1).', '.(int)$row['lastUpdated_day'].', '.(int)$row['lastUpdated_hour'].', '.(int)$row['lastUpdated_minute'];
            if ( $row['killsHardcoreMonsters'] > 0 )
            {
                if (isset($chart_datas[$row['battleTag']]))
                    array_push($chart_datas[$row['battleTag']], array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['killsHardcoreMonsters']));
                else
                    $chart_datas[$row['battleTag']] = array(array(new HighchartJsExpr("Date.UTC($lastUpdated_date)"),(int)$row['killsHardcoreMonsters']));
            }
        }
        break;
    default:
        echo 'WTF ?!';
    }
    unset($query);
    unset($pdo);
    if (!empty($battleTags)) {
        $chart = new Highchart();
        $chart->chart->renderTo = 'graph';
        $chart->chart->type = "spline";
        $chart->title->text = $title;
        $chart->xAxis->type = 'datetime';
        $chart->yAxis->min = 0;
        $chart->yAxis->title->text = $chart_y_title;
        $chart->tooltip->formatter = new HighchartJsExpr( "function() { return '<center><b>'+ this.series.name +'</b><br/>'+ Highcharts.dateFormat('%d/%m/%Y %H:%M', this.x) +'<br/>'+ this.y; }" );
        $battleTags_count = 0;
        foreach ($battleTags as $battleTag) {
            if (!empty($chart_datas[$battleTag])) {
                $battleTag_txt = explode("#", $battleTag);
                $chart->series[]->name = $battleTag_txt[0];
                $chart->series[$battleTags_count]->data = $chart_datas[$battleTag];
                $battleTags_count++;
            }
        }
        unset($battleTags_count);
        echo '<div id="graph"></div>';
    } else {
?>
        <div class="col-lg-12">
            <div class="bs-component">
                <div class="alert alert-dismissible alert-warning">
                    <h4>Pas de données</h4>
                    <p>Aucune données disponible à grapher, pour la saison en cour</p>
                    <p>Début de la prochaine saison le : <?php echo strftime("%A %d %B %Y à %H:%M", strtotime($date_start_cur)); ?></p>

                </div>
            </div>
        </div>
<?php
    }
?>
<br>
<form class="form-inline text-center" role="form" method="get">
<select class="form-control" name="time">
  <option value="day">Derni&egrave;re 48h</option>
  <option value="week">Cette semaine</option>
  <option value="month">Ce mois</option>
</select>
  <button type="submit" class="btn btn-info">ok</button>
</form>
<br>
<?php

} elseif (isset($btags)) {
?>
            <table class="table table-bordered table-striped table-condensed table-hover">
                <thead class="cf">
                    <tr>
                        <th class="text-center" ></th>
                        <th class="text-center" colspan="4">Niveaux Paragon</th>
                        <th class="text-center" colspan="4">Personnages</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th class="text-center info">bTag</th>
                        <th class="text-center success">SC</th>
                        <th class="text-center danger">HC</th>
                        <th class="text-center success">S SC</th>
                        <th class="text-center danger">S HC</th>
                        <th class="text-center success">SC</th>
                        <th class="text-center danger">HC</th>
                        <th class="text-center success">S SC</th>
                        <th class="text-center danger">S HC</th>
                        <th class="text-center info">Date</th>
                    </tr>
                </thead>
                <tbody>
<?php
    $btags_l = implode("','",$btags);
    try {
        $pdo = new PDO ("mysql:host=$mysql_host;dbname=$mysql_bdd","$mysql_user","$mysql_pass");
    } catch (PDOException $e) {
        echo "Failed to get DB handle: " . $e->getMessage() . "\n";
        exit;
    }
    foreach  ($btags as $btag) {
        $sql_career = "SELECT `lastUpdated`,`battleTag`, `paragonLevel`, `paragonLevelHardcore`, `paragonLevelSeason`, `paragonLevelSeasonHardcore`, `killsMonsters`, `killsElites`, `killsHardcoreMonsters`
            FROM `career_data`
            WHERE `battleTag` = '$btag' AND `lastUpdated` = (
                SELECT `lastUpdated`
                FROM `career_data`
                WHERE `battleTag` = '$btag'
                ORDER BY `lastUpdated` DESC LIMIT 1)";
        $sql_heroSC = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `hardcore` = FALSE AND
            `seasonal` = FALSE";
        $sql_heroHC = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `hardcore` = TRUE AND
            `dead`= FALSE AND
            `seasonal` = FALSE";
        $sql_heroHC_D = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `hardcore` = TRUE AND
            `dead`= TRUE AND
            `seasonCreated` <> (SELECT MAX(`seasonCreated`) FROM `hero_data` )";
        $sql_heroSSC = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `hardcore` = FALSE AND
            `seasonCreated` = (SELECT MAX(`seasonCreated`) FROM `hero_data` ) AND
            `seasonal` = TRUE";
        $sql_heroSHC = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `hardcore` = TRUE AND
            `dead`= FALSE AND
            `seasonCreated` = (SELECT MAX(`seasonCreated`) FROM `hero_data` ) AND
            `seasonal` = TRUE";
        $sql_heroSHC_D = "SELECT `id`,`name`
            FROM `hero_data`
            WHERE `battleTag` = '$btag' AND
            `dead`= TRUE AND
            `seasonCreated` = (SELECT MAX(`seasonCreated`) FROM `hero_data` ) AND
            `seasonal` = TRUE";
        $query_career  = $pdo->prepare($sql_career);
        $query_heroSC  = $pdo->prepare($sql_heroSC);
        $query_heroHC  = $pdo->prepare($sql_heroHC);
        $query_heroHC_D  = $pdo->prepare($sql_heroHC_D);
        $query_heroSSC = $pdo->prepare($sql_heroSSC);
        $query_heroSHC = $pdo->prepare($sql_heroSHC);
        $query_heroSHC_D = $pdo->prepare($sql_heroSHC_D);
        $query_career->execute();
        $query_heroSC->execute();
        $query_heroHC->execute();
        $query_heroHC_D->execute();
        $query_heroSSC->execute();
        $query_heroSHC->execute();
        $query_heroSHC_D->execute();
        $row_career = $query_career->fetchAll();
        if ( !empty($row_career[0]['lastUpdated'])) {
            $speudo = explode ("#", $btag);
            echo '<tr>';
            echo '<td data-title="battleTag" id="'.$speudo[0].'-'.$speudo[1].'">';
                echo ' <a href="http://eu.battle.net/d3/fr/profile/'.$speudo[0].'-'.$speudo[1].'/"><img src="http://eu.battle.net/favicon.ico" alt="Battle.Net"></a>';
                echo ' <a href="http://www.diabloprogress.com/player/'.$speudo[0].'-'.$speudo[1].'"><img src="http://DiabloProgress.com/favicon.ico" alt="DiabloProgress.com"></a>';
                echo ' <a href="http://www.diablo3ladder.com/battletag/'.$speudo[0].'-'.$speudo[1].'.html">D3L</a>';
                echo ' - <a href="#'.$speudo[0].'-'.$speudo[1].'">'.$speudo[0].'</a>';
            echo '</td>';
            echo '<td class="numeric text-right" data-title="paragonLevel">'.number_format($row_career[0]['paragonLevel'], 0, ',', ' ').'</td>';
            echo '<td class="numeric text-right" data-title="paragonLevelHardcore">'.number_format($row_career[0]['paragonLevelHardcore'], 0, ',', ' ').'</td>';
            echo '<td class="numeric text-right" data-title="paragonLevelSeason">'.number_format($row_career[0]['paragonLevelSeason'], 0, ',', ' ').'</td>';
            echo '<td class="numeric text-right" data-title="paragonLevelSeasonHardcore">'.number_format($row_career[0]['paragonLevelSeasonHardcore'], 0, ',', ' ').'</td>';
            if (strtotime($row_career[0]['lastUpdated']) <= strtotime('-15 days')) {
                $lastUpdated_color='danger';
            } elseif (strtotime($row_career[0]['lastUpdated']) <= strtotime('-5 days')) {
                $lastUpdated_color='warning';
            } else {
                $lastUpdated_color='success';
            }
            echo '<td class="numeric text-center" data-title="PersosSC">'.$query_heroSC->rowCount().'</td>';
            echo '<td class="numeric text-center" data-title="PersosHC">'.$query_heroHC->rowCount().' <s>('.$query_heroHC_D->rowCount().')</s></td>';
            echo '<td class="numeric text-center" data-title="PersosSSC">'.$query_heroSSC->rowCount().'</td>';
            echo '<td class="numeric text-center" data-title="PersosSHC">'.$query_heroSHC->rowCount().' <s>('.$query_heroSHC_D->rowCount().')</s></td>';
            echo '<td class="numeric text-center '.$lastUpdated_color.'" data-title="lastUpdated">'.date('d/m/y H\hi', strtotime($row_career[0]['lastUpdated'])).'</td>';
        }
    }
    unset($query_career);
    unset($row_career);
    unset($pdo);
?>
                </tbody>
            </table>
<?php
} elseif ( isset($youtube_lastvid)) {
    echo '<div class="flex-video widescreen">';
    echo '<object>';
    echo '<param name="movie" value="http://www.youtube.com/v/'.$youtube_lastvid.'?version=3&amp;fs=1&amp;autohide=1&amp;theme=dark&amp;color=white&amp;modestbranding=1&amp;showinfo=0&amp;rel=0&amp;playsinline=1">';
    echo '<param name="allowFullScreen" value="true">';
    echo '<param name="allowScriptAccess" value="always">';
    echo '<embed src="http://www.youtube.com/v/'.$youtube_lastvid.'?version=3&amp;fs=1&amp;autohide=1&amp;theme=dark&amp;color=white&ampmodestbranding=1&amp;showinfo=0&amp;rel=0&amp;playsinline=1" type="application/x-shockwave-flash" allowfullscreen="true"';
    echo 'allowscriptaccess="always">';
    echo '</object>';
    echo '</div>';
} elseif ( $_SERVER['REQUEST_URI']=="/twitch" ) {
    if ( $twitch_online == TRUE ) {
        echo '<div class="flex-video widescreen"><iframe src="'.$twitch_url.'/embed" allowfullscreen></iframe></div>';
    } else {
        echo 'Offline';
    }
} elseif ( $default == TRUE ) {
?>
<div class="jumbotron">
    <h1>Pad's Diablo III Minisite</h1>
    <p>Hello, et bienvenue sur le petit site fait avec <a href="http://www.vim.org/">VIM</a></p>
    <p>Vous y trouverez :
    <ul>
        <li>Le live Twitch <i class="fa fa-twitch"></i> si je joue :)</li>
        <li>Les replays YouTube <i class="fa fa-youtube"></i></li>
        <li>Les stats <i>vite fait</i> de ma friend liste</li>
        <li>Les stats <i>toujours vite fait</i> de mon clan</li>
</ul>
</div>
<div class="jumbotron">
<h2>ChangeLog</h2>
<ul>
<?php
$git_logs=array();
exec('git --no-pager log --format="%s" -5', $git_logs);
foreach ( $git_logs as $git_log ) {
    if (!empty($git_log) )
        echo '<li>'.htmlentities($git_log).'</li>';
}
?>
</ul>
</div>
<div class="jumbotron">
<h2>Sources</h2>
<ul>
    <li>Site with Docker image : <a href="https://git.depad.fr/depad/d3">Pad's GitLab</a></li>
    <li>Class PHP : <a href="https://github.com/XjSv/Diablo-3-API-PHP">XjSv/Diablo-3-API-PHP</a></li>
    <li>Twitter Bootstram : <a href="http://getbootstrap.com/">http://getbootstrap.com/</a></li>
</ul>
</div>
<?php
}
?>
</div>
    <footer class="row">
        <div class="panel-footer text-center jumbotron">
<?php
$unit=array('o','Ko','Mo','Go','To','Po');
echo 'Memoire utilisée : '.@round(memory_get_usage(true)/pow(1024,($i=floor(log(memory_get_usage(true),1024)))),2).' '.$unit[$i];
?>
        </div>
    </footer>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <?php if ($page_type == 'stats') { ?>
    <script type="text/javascript" src="//code.highcharts.com/highcharts.js"></script>
    <script type="text/javascript" src="http://code.highcharts.com/modules/exporting.js"></script>
    <script type="text/javascript"><?php echo $chart->render("chart0"); ?></script>
    <?php } ?>
    <script type="text/javascript" src="http://us.battle.net/d3/static/js/tooltips.js"></script>
</body>
</html>
