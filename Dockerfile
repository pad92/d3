# Stage 1:
# - Get sources
# - Resolve PHP dependencies with Composer
FROM composer:latest as composer
RUN curl -L https://git.depad.fr/depad/d3/-/archive/master/d3-master.tar.gz | tar xzf - \
 && mv d3-master d3 \
 && cd d3/www \
 && composer --prefer-dist --no-dev install
# Stage 2:
# - build image
FROM alpine:3.9
LABEL maintainer="pad"
LABEL version="$CI_COMMIT_REF_NAME"
RUN apk --update --no-cache add \
        ca-certificates \
        nginx \
        php7 \
        php7-ctype \
        php7-curl \
        php7-fpm \
        php7-pdo_mysql \
        php7-mysqli \
        php7-iconv \
        php7-intl \
        php7-json \
        php7-mbstring \
        php7-openssl \
        php7-session \
        php7-simplexml \
        php7-xml \
        php7-zlib \
        git \
        s6

COPY conf/nginx.conf /etc/nginx/nginx.conf
COPY conf/php-fpm.conf /etc/php7/php-fpm.conf
COPY conf/services.d /etc/services.d

RUN rm -rf /etc/php7/php-fpm.d/www.conf

WORKDIR /var/www
COPY --from=composer /app/d3/www d3
RUN mv d3/config.db.inc.php-sample d3/config.db.inc.php \
 && ln -sf /dev/stderr /var/log/fpm-php.www.log \
 && ln -sf /dev/stdout /var/log/nginx/d3.access.log \
 && ln -sf /dev/stderr /var/log/nginx/d3.error.log \
 && echo '*/5 * * * * /usr/bin/php7 /var/www/d3/cron.php 1>/dev/stdout 2>/dev/stderr' > /var/spool/cron/crontabs/root

EXPOSE 80

ENTRYPOINT ["/bin/s6-svscan", "/etc/services.d"]